package com.pe.navegacion;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class DestinoActivity extends AppCompatActivity {

    TextView tviMensaje;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_destino);

        tviMensaje = findViewById(R.id.tviMensaje);

        //Obtenemos los valores del intent del MainActivity
        Intent intent = getIntent();
        String mensaje = intent.getStringExtra("nombre");
        Pedro pedro = (Pedro) intent.getSerializableExtra("objeto");
        String valor = intent.getStringExtra("wewesa");

        int edad = intent.getIntExtra("edad", 0);


        tviMensaje.setText(mensaje + edad + " " + pedro.habilidades + " " + valor);

    }
}
